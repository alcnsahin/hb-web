# Hepsiburada Web Application

This is a simple node.js application.

#### Before running the app manually you need to install all the required npm packages:
`npm install`<br>

#### Run the app
`node main.js`

#### Run as Docker container
`docker build -t hb-web .`<br>
`docker run -d -p 11130:3000 --name hb-web-api hb-web`

#### Deploy to k8s cluster
`kubectl create -f deployment/deployment.yml`<br>
`kubectl create -f deployment/svc.yml`<br>
`kubectl create -f deployment/autoscale.yml`