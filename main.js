const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const PORT = 3000;

app.use(bodyParser.json());

app.listen(PORT, () => {
    console.log("Server running on port " + PORT);
});

app.get("/api/health-check", (req, res, next) => {
    res.status(200).send("SUCCESS");
    next()
});

app.get("/", async (req, res, next) => {
    res.status(200).send("Hello Hepsiburada from @alcnsahin");
    next();
});